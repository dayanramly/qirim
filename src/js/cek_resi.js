main();

function init($) {
	setCarriersData($);
}

function main() {
	$(document).ready(function ($) {
		init($);
	});
}


$('#zuragan-resi-form').submit(function (event) {
	event.preventDefault();
	let $inputs = $('#zuragan-resi-form :input');
	getWaybillDetails($, $inputs);

});

function setCarriersData($) {
	$.ajax({
		url: baseUrl + "/api/v3/carriers/list",
		dataType: "JSON",
		type: "GET",
		async: true,
		success: function (response) {
			console.log(response);
			let carriersForm = $('#ongkir_carrier');
			carriersForm.empty();
			$.each(response.data, function (key, value) {
				if (value['is_track_shipment_on'] == 1){
					carriersForm.append('<option value=' + value['code'] + '>' + value['name'] + '</option>');
				}
			});
		}
	});
}

function getWaybillDetails($, $inputs) {
	// console.log($inputs);
	$('#qirim-resi-result').addClass("loading");
	$(':button[type="submit"]').prop('disabled', true);

	let data = {};
	let values = {};

	$inputs.each(function () {
		values[this.name] = {
			value: $(this).val(),
			id: $(this).attr('data-id'),
			type: $(this).attr('data-type')
		};
	});
	data.waybill = values.no_resi.value;
	data.courier = values.ongkir_carrier.value;

	// console.log(data.waybill);
	// console.log(data.courier);

	$.ajax({
		url: baseUrl + "/api/v3/ongkir/track-shipment",
		data: data,
		dataType: "JSON",
		type: "POST",
		async: true,
		success: function (response) {
			console.log(response);
			$('#qirim-resi-result').removeClass("loading");
			let responseData = response.data.result;
			let resultStatus = responseData.summary.status;

			let resultHtml = $('#qirim-resi-result');
			let resultHtmlHeader = $('#resi-results-header');
			let resultHtmlContent = $('#resi-results-content');
			resultHtml.addClass("show");
			// resultHtmlContent.append(JSON.stringify(responseData));
			let statusType = ``;
			let serviceCode = ``;

			// - urutan hasil => Tgl Pengiriman, Kurir, Layanan, No. Resi, Nama Pengirim, Penerima, Status, Tgl Diterima

			if (resultStatus == "ON PROCESS" || resultStatus == "Dalam Proses"){
				statusType = `<div class="status--content"><p class="status--content-label">Status</p> <label class="badge badge-outline-warning">`+ responseData.summary.status +`</label></div>`
			} else{
				statusType = `
				<div class="status--content"><p class="status--content-label">Tujuan</p><p class="status--content-text text-name">`+ responseData.summary.receiver_name.toUpperCase() +`</p> <p class="status--content-text">`+ responseData.summary.destination +`</p></div>
				<div class="status--content"><p class="status--content-label">Status</p> <label class="badge badge-outline-success">`+ responseData.summary.status +`</label></div>
				<div class="status--content"><p class="status--content-label">Tgl Diterima</p><p class="status--content-text">`+ $.format.date(responseData.delivery_status.pod_date + ` ` + responseData.delivery_status.pod_time, "dd MMM yyyy") +`</p></div>
				<div class="status--content"><p class="status--content-label">Nama Penerima</p><p class="status--content-text">`+ responseData.delivery_status.pod_receiver.toUpperCase() +`</p></div>
				`
			}

			if(responseData.summary.service_code == null || responseData.summary.service_code == ""){
				serviceCode = " - ";
			} else{
				serviceCode = responseData.summary.service_code;
			}

			let resiHeaderSearch = `
			<h2>Hasil Lacak Resi</h2>
			<div class="d-flex resi-results-header--status flex-row justify-content-center">
			<div class="status--content"><p class="status--content-label">Tgl Pengiriman</p><p class="status--content-text">`+ $.format.date(responseData.summary.waybill_date + ` ` + responseData.summary.waybill_time, "dd MMM yyyy") +`</p></div>
			<div class="status--content"><p class="status--content-label" style="line-height: normal;">Kurir</p><img src="`+ responseData.summary.courier_logo +`"></div>
			<div class="status--content"><p class="status--content-label">Layanan</p><p class="status--content-text">`+ serviceCode +`</p></div>
			<div class="status--content"><p class="status--content-label">Nomor AWB</p><p class="status--content-text">`+ responseData.summary.waybill_number +`</p></div>
			<div class="status--content"><p class="status--content-label">Asal</p><p class="status--content-text text-name">`+ responseData.summary.shipper_name.toUpperCase() +`</p><p class="status--content-text">`+ responseData.summary.origin +`</p></div>
			`+ statusType +`
			</div>
			`;
			resultHtmlHeader.html(resiHeaderSearch);

			let table = `

			<ul class="resi-timeline px-md-5">
			</ul>
			`;

			if (responseData.manifest !== null || responseData.manifest !== undefined) {
				if (responseData.manifest.length > 0) {
					resultHtmlContent.html(table);

					for (var j = 0; j < responseData.manifest.length; j++) {
						$('.resi-timeline').append(`
							<li class="timeline-item">
							<div class="timeline-item--badge"><i class="icon ion-md-radio-button-on"></i></div>
							<div class="timeline-item--panel">
							<div class="timeline-item--panel-heading">
							<p class="panel-heading--date">` + $.format.date(responseData.manifest[j].manifest_date + ` ` + responseData.manifest[j].manifest_time, "dd MMM yyyy") + `</p>
							<p class="panel-heading--time">` + responseData.manifest[j].manifest_time + `</p>
							</div>
							<div class="timeline-item--panel-body">
							<p>` + responseData.manifest[j].manifest_description + `</p>
							</div>
							</div>
							</div>
							</li>`
							);
					}
					$('.resi-timeline').append(`<li class="timeline-item">
						<div class="timeline-item--badge"><i class="icon ion-md-radio-button-off"></i></div>
						</li>`);
				}
				else {
					$('#resi-results').empty();
					resultHtml.append('<p style="text-align: center">Hasil pencarian tidak ditemukan..</p>');
				}
			}
		}
	});

$(':button[type="submit"]').prop('disabled', false);

$('html, body').animate({
	scrollTop: $('#qirimContent').offset().top 
}, 500, 'linear');
}

function formatCurrency(total) {

	var dot = ".";
	var value = new String(total);
	var decimal = [];
	while (value.length > 3) {
		var asd = value.substr(value.length - 3);
		decimal.unshift(asd);
		value = value.substr(0, value.length - 3);
	}

	if (value.length > 0) {
		decimal.unshift(value);
	}
	value = decimal.join(dot);
	return value;

}