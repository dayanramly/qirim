'use strict';


let jQuery;
let baseUrl = "https://ongkir.zuragan.com";
let clipboard = new ClipboardJS('.btn');

let cities_cat = $.map(cities, function (cities_name) { return { value: cities_name.name, data: { id: cities_name.id, category: 'Kabupaten/Kota', category_en: 'city' }}; });
let subs_cat = $.map(subdistricts, function (subdistricts_name) { return { value: subdistricts_name.name, data: { id: subdistricts_name.id, category: 'Kecamatan', category_en: 'subdistrict' }}; });

let mix_all = cities_cat.concat(subs_cat);


$('#ship_from').devbridgeAutocomplete({
	lookup: mix_all,
	showNoSuggestionNotice: true,
	lookupLimit: 20,
	autoSelectFirst: true,
	groupBy: 'category',
	noSuggestionNotice: '<p class="p-2" style="font-style:italic;">Maaf, Kota tidak ditemukan</p>',
	onSelect: function (suggestion) {
		$('#ship_from').attr({
			"data-id" : suggestion.data.id,
			"data-type" : suggestion.data.category_en
		});
		$('#ship_from').addClass("border-main");
	},
	onHide: function (container) {
		if(container[0].textContent == "Maaf, Kota tidak ditemukan") {
			$('#ship_from').val('');
			$('#ship_from').removeClass("border-main").addClass("border-red");
			$('#ship_from_alert').show();
		} else{
			$('#ship_from').removeClass("border-red").addClass("border-main");
			$('#ship_from_alert').hide();
		}
	}
});

$('#ship_dest').devbridgeAutocomplete({
	lookup: mix_all,
	showNoSuggestionNotice: true,
	lookupLimit: 15,
	autoSelectFirst: true,
	groupBy: 'category',
	noSuggestionNotice: '<p class="p-2" style="font-style:italic;">Maaf, Kota/kecamatan tidak ditemukan</p>',
	onSelect: function (suggestion) {
		$('#ship_dest').attr({
			"data-id" : suggestion.data.id,
			"data-type" : suggestion.data.category_en
		});
		$('#ship_dest').addClass("border-main");
	},	
	onHide: function (container) {
		if(container[0].textContent == "Maaf, Kota/kecamatan tidak ditemukan") {
			$('#ship_dest').val('');
			$('#ship_dest').removeClass("border-main").addClass("border-red");
			$('#ship_dest_alert').show();
		} else{
			$('#ship_dest').removeClass("border-red").addClass("border-main");
			$('#ship_dest_alert').hide();
		}
	}
});

$('#zuragan-ongkir-form').submit(function (event) {
	event.preventDefault();
	let $inputs = $('#zuragan-ongkir-form :input');
	getShippingCosts($, $inputs);

});

function getShippingCosts($, $inputs) {
	$('#qirim-result').addClass("loading");
	$(':button[type="submit"]').prop('disabled', true);
	let data = {};
	let values = {};

	$inputs.each(function () {
		values[this.name] = {
			value: $(this).val(),
			id: $(this).attr('data-id'),
			type: $(this).attr('data-type')
		};
	});

	data.origin = values.ship_from.id;
	data.originType = values.ship_from.type;
	data.destination = values.ship_dest.id;
	data.destinationType = values.ship_dest.type;
	data.weight = values.weight.value;
	data.courier = "jne:pos:tiki:wahana:jnt:pandu:sicepat";
	data.length = 0;
	data.width = 0;
	data.height = 0;

	// console.log(data);

	$.ajax({
		url: baseUrl + "/api/v3/ongkir/shipping-costs?is_group=true&sort=value.asc",
		data: data,
		dataType: "JSON",
		type: "POST",
		async: true,
		success: function (response) {
			$('#qirim-result').removeClass("loading");
			// console.log(response);
			let responseData = response.data;
			let resultLength = responseData.results;

			let resultHtml = $('#qirim-result');
			let resultHtmlHeader = $('#ongkir-results-header');
			let resultHtmlContent = $('#ongkir-results-content');
			resultHtml.addClass("show");

			let destType = responseData.destination_details.type;
			// console.log(destType);
			let viewDestType = ``;

			if (destType == "city"){
				viewDestType = `<div class="header-dest py-md-0 px-md-2"><p>`+ responseData.destination_details.city_name +`, `+ responseData.destination_details.province +` <span class="header-weight">(`+ responseData.query.weight +` gram)</span></p></div>`;
			} else{
				viewDestType = `<div class="header-dest py-md-0 px-md-2"><p>`+ responseData.destination_details.subdistrict_name +`, `+ responseData.destination_details.city_name +` `+ responseData.destination_details.province +` <span class="header-weight">(`+ responseData.query.weight +`gram)</span></p></div>`;
			};

			let headerSearch = `
			<h2>Hasil Pencarian</h2>
			<div class="d-flex flex-row justify-content-center ongkir-results-header--wrapper">
			<div class="header-origin py-md-0 px-md-2"><p>`+ responseData.origin_details.city_name +`, `+ responseData.origin_details.province +`</p></div>
			<div class="header-arrow py-md-0 px-md-2"><i class="icon ion-md-arrow-round-forward"></i></div>
			`+ viewDestType +`
			</div>
			`;
			resultHtmlHeader.html(headerSearch);

			let table = `
			<div class="ongkir-result">
				<div class="row ongkir-result--row title-row align-items-center">
					<div class="col-md-3 ongkir-result--row-img"><h3>Kurir</h3></div>
					<div class="col-md-7 ongkir-result--row-list">
						<div class="row-list--wrapper d-flex align-items-center">
							<div class="row-list--service"><h3>Layanan</h3></div>
							<div class="row-list--price"><h3>Harga</h3></div>
						</div>
					</div>
					<div class="col-md-2 ongkir-result--row-copy"></div>
				</div>
			</div>
			`;

			if (responseData !== null || responseData !== undefined) {
				if (responseData.results.length > 0) {
					resultHtmlContent.html(table);

					let content_row = ``;
					let clipboard_row = ``;

					for (var j = 0; j < resultLength.length; j++) {

						let content_list = ``;
						let clipboard_list = ``;
						let clipboard_list_all = ``;

						for (var serv = 0; serv < resultLength[j].services.length; serv++) {
							let content = `
							<div class="row-list--wrapper d-flex align-items-top">
							<div class="row-list--service"><h4>` + resultLength[j].services[serv].service + `</h4></div>
							<div class="row-list--price"><sup style="font-size:11px; color:#4d4d4d">Rp. </sup>` + formatCurrency(resultLength[j].services[serv].value.toFixed(0)) + `<p>`+resultLength[j].services[serv].etd+`</p></div>
							</div>
							`;

							clipboard_list = resultLength[j].services[serv].code.toUpperCase() + ` ` + resultLength[j].services[serv].service +` • Rp. `+ formatCurrency(resultLength[j].services[serv].value.toFixed(0)) + `, ` + resultLength[j].services[serv].etd + ` • `;

							content_list = content_list.concat(content);
							clipboard_list_all = clipboard_list_all.concat(clipboard_list);
						}

						clipboard_row = clipboard_row.concat(clipboard_list_all)
						// console.log(clipboard_list_all);
						// console.log(clipboard_row);

						content_row = `
						<div class="row ongkir-result--row">
						<div class="col-md-3 ongkir-result--row-img">
						<img src="` + resultLength[j].logo + `"/>
						</div>
						<div class="col-md-7 ongkir-result--row-list">`+content_list+`</div>
						<div class="col-md-2 ongkir-result--row-copy">
						<button id="`+ resultLength[j].code +`" data-clipboard-text="`+responseData.origin_details.city_name +` -> ` + responseData.destination_details.city_name +` (`+ responseData.query.weight +`gram) • `+ clipboard_list_all +`" class="btn btn-sm btn-outline-main" data-toggle="tooltip" data-placement="bottom" title="Copied!" ><i class="icon ion-md-copy"></i> Copy</button>
						</div>
						</div>
						`;
						// console.log(content_row);

						$('.ongkir-result').append(content_row);

					}

				} else {
					$('#qirim-result').empty();
					resultHtml.append('<p style="text-align: center">Hasil pencarian tidak ditemukan..</p>');
				}
			}

			$(':button[type="submit"]').prop('disabled', false);

			$('html, body').animate({
				scrollTop: $('#qirimContent').offset().top 
			}, 500, 'linear');

			$('#ship_dest').val('');
			$('#ship_dest').removeClass("border-main");

		}
	});

function formatCurrency(total) {

	var dot = ".";
	var value = new String(total);
	var decimal = [];
	while (value.length > 3) {
		var asd = value.substr(value.length - 3);
		decimal.unshift(asd);
		value = value.substr(0, value.length - 3);
	}

	if (value.length > 0) {
		decimal.unshift(value);
	}
	value = decimal.join(dot);
	return value;

}


function setTooltip(id, message) {
	console.log();
	$('#'+id).tooltip('hide')
	.attr('data-original-title', message)
	.tooltip('show');
}

function hideTooltip() {
	setTimeout(function() {
		$('button').tooltip('hide');
	}, 1000);
}

clipboard.on('success', function(e) {
	setTooltip(e.trigger.id, 'Copied!');
	hideTooltip();

	e.clearSelection();
});

clipboard.on('error', function(e) {
	console.error('Action:', e.action);
	console.error('Trigger:', e.trigger);
});


}